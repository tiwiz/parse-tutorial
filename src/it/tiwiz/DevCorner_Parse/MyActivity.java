package it.tiwiz.DevCorner_Parse;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.parse.*;

import java.util.List;

public class MyActivity extends Activity implements View.OnClickListener {
    /**
     * Called when the activity is first created.
     */

    private final static String APPLICATION_ID = "pZWBJ3VEwKezycsdupvfTZeUOnBwpbA82H4VsojK";
    private final static String CLIENT_KEY = "zBzCNaI2wmtmlnIWLuaC2khZaJmUV1Q1yrtWRdYW";

    private Button btnSave;
    private Button btnRetrieve;
    private Button btnDelete;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //inizializziamo Parse
        Parse.initialize(this, APPLICATION_ID,CLIENT_KEY );

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        btnRetrieve = (Button) findViewById(R.id.btnRetrieve);
        btnRetrieve.setOnClickListener(this);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.btnSave:
                saveToParse();
                break;
            case R.id.btnRetrieve:
                retrieveFromParse();
                break;
            case R.id.btnDelete:
                deleteFromParse();
                break;
        }
    }

    private void saveToParse(){

        //creiamo un nuovo oggetto di tipo DevCorner
        ParseObject obj = new ParseObject("DevCorner");
        //gli assegnamo i dati che ci interessano come coppia chiave-valore
        obj.put("BestBlog","AndroidWorld.it");
        //e lo salviamo
        obj.saveInBackground();
    }

    private void retrieveFromParse(){

        //creo la query per il tipo di oggetto che ci interessa
        ParseQuery<ParseObject> query = ParseQuery.getQuery("DevCorner");
        //imposto i parametri della ricerca
        query.whereEqualTo("BestBlog","AndroidWorld.it");
        //ricerco in background
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if(e==null){
                    for(ParseObject obj : parseObjects){ //e svolgo le operazioni che mi servono sui miei oggetti
                        Toast.makeText(getApplicationContext(),"Il miglior blog del mondo è " + obj.get("BestBlog"),Toast.LENGTH_SHORT).show();
                    }
                }else
                    Toast.makeText(getApplicationContext(),"Eccezione: " + e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteFromParse(){

        //creo la query per il tipo di oggetto che ci interessa
        ParseQuery<ParseObject> query = ParseQuery.getQuery("DevCorner");
        //imposto i parametri della ricerca
        query.whereEqualTo("BestBlog","AndroidWorld.it");
        //ricerco in background
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if(e==null){
                    for(ParseObject obj : parseObjects){ //e svolgo le operazioni che mi servono sui miei oggetti
                        obj.deleteInBackground();
                    }

                    Toast.makeText(getApplicationContext(),"Ho inviato la richiesta di cancellazione per gli oggetti",Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(getApplicationContext(),"Eccezione: " + e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
